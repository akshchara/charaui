all: gui

gui:
	$(MAKE) -C charaUI/res

run:
	python -m charaUI -v

doc:
	$(MAKE) html -C docs

.PHONY: clean
clean:
	$(MAKE) clean -C charaUI/res
	$(MAKE) clean -C docs
	rm -rf charaUI/*.pyc
	rm -rf charaUI/__pycache__
	rm -rf data