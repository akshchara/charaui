from charaUI.ui.mainWindow_ui import *

from charaUI.core.worker import Worker
from charaUI.core.constants import Constants, SourceType
from charaUI.ui.popUp import PopUp
from charaUI.common.logger import Logger as Log
from time import sleep
import random
from PyQt5.QtWidgets import QFileDialog
import csv
import time

addressList = []
dataList    = []

TAG = "UI"


class MainWindow(QtGui.QMainWindow):
    """
    Handles the ui elements and connects to worker service to execute processes.
    """
    def __init__(self, port=None, bd=115200, samples=500):
        """
        Initializes values for the UI.
        :param port: Default port name to be used. It will also disable scanning available ports.
        :type port: str.
        :param bd: Default baud rate to be used. It will be added to the common baud rate list if not available.
        :type bd: int.
        :param samples: Default samples per second to be shown in the plot.
        :type samples: int.
        """
        QtGui.QMainWindow.__init__(self)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        # Shared variables, initial values
        self._plt = None
        self._timer_plot = None
        self._timer_data_update = None
        self.worker = Worker()

        # configures
        self.ui.cBox_Source.addItems(Constants.app_sources)
        self._configure_plot()
        self._configure_timers()
        self._configure_signals()

        # populate combo box for serial ports
        self._source_changed()
        self.ui.cBox_Source.setCurrentIndex(SourceType.serial.value)

        self.ui.sBox_Samples.setValue(samples)

        # enable ui
        self._enable_ui(True)

    def start(self):
        """
        Starts the acquisition of the selected serial port.
        This function is connected to the clicked signal of the Start button.
        :return:
        """
        Log.i(TAG, "Clicked start")
        self.worker = Worker(port=self.ui.cBox_Port.currentText(),
                             speed=float(self.ui.cBox_Speed.currentText()),
                             samples=self.ui.sBox_Samples.value(),
                             source=self._get_source(),
                             export_enabled=self.ui.chBox_export.isChecked())
        if self.worker.start():
            self._timer_plot.start(Constants.plot_update_ms)
            self._timer_data_update.start(500)
            self._enable_ui(False)
        else:
            Log.i(TAG, "Port is not available")
            PopUp.warning(self, Constants.app_title, "Selected port \"{}\" is not available"
                          .format(self.ui.cBox_Port.currentText()))

    def stop(self):
        """
        Stops the acquisition of the selected serial port.
        This function is connected to the clicked signal of the Stop button.
        :return:
        """
        Log.i(TAG, "Clicked stop")
        self._timer_plot.stop()
        self._timer_data_update.stop()
        self._enable_ui(True)
        self.worker.stop()

    def closeEvent(self, evnt):
        """
        Overrides the QTCloseEvent.
        This function is connected to the clicked signal of the close button of the window.
        :param evnt: QT evnt.
        :return:
        """
        if self.worker.is_running():
            Log.i(TAG, "Window closed without stopping capture, stopping it")
            self.stop()

    def _enable_ui(self, enabled):
        """
        Enables or disables the UI elements of the window.
        :param enabled: The value to be set at the enabled characteristic of the UI elements.
        :type enabled: bool
        :return:
        """
        self.ui.cBox_Port.setEnabled(enabled)
        self.ui.cBox_Speed.setEnabled(enabled)
        self.ui.pButton_Start.setEnabled(enabled)
        self.ui.chBox_export.setEnabled(enabled)
        self.ui.cBox_Source.setEnabled(enabled)
        self.ui.pButton_Stop.setEnabled(not enabled)

    def _configure_plot(self):
        """
        Configures specific elements of the PyQtGraph plots.
        :return:
        """
        self.ui.plt.setBackground(background=None)
        self.ui.plt.setAntialiasing(True)
        self._plt = self.ui.plt.addPlot(row=1, col=1)
        self._plt.setLabel('bottom', Constants.plot_xlabel_title, Constants.plot_xlabel_unit)

    def _configure_timers(self):
        """
        Configures specific elements of the QTimers.
        :return:
        """
        self._timer_plot = QtCore.QTimer(self)
        self._timer_plot.timeout.connect(self._update_plot)
        self._timer_data_update = QtCore.QTimer(self)
        self._timer_data_update.timeout.connect(self._update_varParamdata)

    startStopTest = 0
    count = 0
    randomInt = 0
    startStopSpeed = 0
    failed_start = 0
    total_start = 0

    def  _update_varParamdata(self):        
        self.update_param_val(self.worker.get_paramrx_data())
        self.update_var_val(self.worker.get_varrx_data())


    def _configure_signals(self):
        """
        Configures the connections between signals and UI elements.
        :return:
        """
        self.ui.pButton_Start.clicked.connect(self.start)
        self.ui.pButton_Stop.clicked.connect(self.stop)
        self.ui.sBox_Samples.valueChanged.connect(self._update_sample_size)
        self.ui.cBox_Source.currentIndexChanged.connect(self._source_changed)

        self.ui.paramSet.clicked.connect(self._set_param)
        self.ui.paramGet.clicked.connect(self._get_param)
        self.ui.VarSet.clicked.connect(self._set_var)
        self.ui.VarGet.clicked.connect(self._get_var)
        self.ui.SetupStream.clicked.connect(self._setup_stream)
        self.ui.pButton_Params.clicked.connect(self._paramsUpdate)
        self.ui.pButton_Update.clicked.connect(self._Update)

    def _paramsUpdate(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        global fileName
        fileName, _ = QFileDialog.getOpenFileName(self, "QFileDialog.getOpenFileName()", 
            "","All Files (*);;Python Files (*.py)", options=options)

    def _Update(self):
        counter = 0 
        with open(fileName, 'r') as file:
            reader = csv.reader(file)
            for row in reader:
                counter = counter + 1;
                if(counter>1):
                    if(type(row[0])=='str'):
                        row[0]=int(row[0])
                    Log.i(TAG, "SET_PARAM ADDR = {}, DATA = {}".format(row[0],row[1]))
                    addressList.append(row[0])
                    dataList.append(row[1])
                    self.worker.send_set_param(int(row[0],16), int(row[1]))
            print("\n*** PARM WRITE COMPLETED ***")
            self.worker.verifyWrite(addressList,dataList,(counter-1))

    def _update_sample_size(self):
        """
        Updates the sample size of the plot.
        This function is connected to the valueChanged signal of the sample Spin Box.
        :return:
        """
        if self.worker is not None:
            Log.i(TAG, "Changing sample size")
            self.worker.reset_buffers(self.ui.sBox_Samples.value())

    def _update_plot(self):
        """
        Updates and redraws the graphics in the plot.
        This function us connected to the timeout signal of a QTimer.
        :return:
        """
        self.worker.consume_queue()

        # plot data
        self._plt.clear()
        for idx in range(self.worker.get_lines()):
            self._plt.plot(x=self.worker.get_time_buffer(),
                           y=self.worker.get_values_buffer(idx),
                           pen=Constants.plot_colors[idx])

    def _source_changed(self):
        """
        Updates the source and depending boxes on change.
        This function is connected to the indexValueChanged signal of the Source ComboBox.
        :return:
        """
        Log.i(TAG, "Scanning source {}".format(self._get_source().name))
        # clear boxes before adding new
        self.ui.cBox_Port.clear()
        self.ui.cBox_Speed.clear()

        source = self._get_source()
        ports = self.worker.get_source_ports(source)
        speeds = self.worker.get_source_speeds(source)

        if ports is not None:
            self.ui.cBox_Port.addItems(ports)
        if speeds is not None:
            self.ui.cBox_Speed.addItems(speeds)
        if self._get_source() == SourceType.serial:
            self.ui.cBox_Speed.setCurrentIndex(len(speeds) - 1)

    def _get_source(self):
        """
        Gets the current source type.
        :return: Current Source type.
        :rtype: SourceType.
        """
        return SourceType(self.ui.cBox_Source.currentIndex())

    def _set_param(self):
        param_addr = self.ui.paramAddr.text()
        param_data = self.ui.paramData.text() 
        param_addr_hex = int(param_addr, 16)
        param_data_int = int(param_data)
        Log.i(TAG, "SET_PARAM ADDR = {}, DATA = {}".format(param_addr_hex, param_data_int))
        self.worker.send_set_param(param_addr_hex, param_data_int)

        # Motor start test
        if (param_addr_hex == 0xFF):
            if (param_data_int == 1):
                self.startStopTest = 1
                self.worker.send_set_variable(0x1, 1100)
                self.count = 0
                print("STARTING MOTOR TEST")
            else:
                self.startStopTest = 0
                self.count = 0
                print("STOPPING MOTOR TEST")

    def _get_param(self):
        param_addr = self.ui.paramAddr.text()
        param_addr_hex = int(param_addr, 16)
        Log.i(TAG, "GET_PARAM {}".format(param_addr_hex))
        self.worker.send_get_param(param_addr_hex)

    def _set_var(self):
        var_addr = self.ui.VarAddr.text()
        var_data = self.ui.VarData.text()
        var_addr_hex = int(var_addr, 16)
        var_data_int = int(var_data)
        Log.i(TAG, "SET_VAR ADDR = {}, DATA = {}".format(var_addr_hex, var_data_int))
        self.worker.send_set_variable(var_addr_hex, var_data_int) 

    def _get_var(self):
        var_addr = self.ui.VarAddr.text()
        var_addr_hex = int(var_addr, 16)
        Log.i(TAG, "GET_VAR ADDR = {}".format(var_addr_hex))
        self.worker.send_get_variable(var_addr_hex)

    def update_param_val(self, val):
        if (val is not None):
            Log.i(TAG, "UPDATE PARAM DATA = {}".format(val))
            self.ui.paramGetData.setText(str(val))

    def update_var_val(self, val):
        if (val is not None):
            #Log.i(TAG, "UPDATE VAR DATA = {}".format(val))
            self.ui.VarGetData.setText(str(val))
            self.startStopSpeed = val                    

    def _setup_stream(self):
        addr1 = int(self.ui.StrmAddr1.text(), 16)
        addr2 = int(self.ui.StrmAddr2.text(), 16)
        addr3 = int(self.ui.StrmAddr3.text(), 16) 
        addr4 = int(self.ui.StrmAddr4.text() , 16)
        addr5 = int(self.ui.StrmAddr5.text(), 16)
        time = int(self.ui.StreamTime.text())
        Log.i(TAG, "SETUP STREAM ADDR = {} {} {} {} {} TIME = {}".format(addr1, addr2, addr3, addr4, addr5, time))
        self.worker.send_setup_stream(addr1, addr2, addr3, addr4, addr5, time)        
