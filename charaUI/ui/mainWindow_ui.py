# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainWindow.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1204, 916)
        MainWindow.setMinimumSize(QtCore.QSize(0, 0))
        MainWindow.setStyleSheet("")
        MainWindow.setTabShape(QtWidgets.QTabWidget.Rounded)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.Layout_controls = QtWidgets.QGridLayout()
        self.Layout_controls.setObjectName("Layout_controls")
        self.cBox_Speed = QtWidgets.QComboBox(self.centralwidget)
        self.cBox_Speed.setEditable(True)
        self.cBox_Speed.setObjectName("cBox_Speed")
        self.Layout_controls.addWidget(self.cBox_Speed, 1, 1, 1, 1)
        self.pButton_Stop = QtWidgets.QPushButton(self.centralwidget)
        self.pButton_Stop.setObjectName("pButton_Stop")
        self.Layout_controls.addWidget(self.pButton_Stop, 1, 3, 1, 1)
        self.cBox_Port = QtWidgets.QComboBox(self.centralwidget)
        self.cBox_Port.setEditable(True)
        self.cBox_Port.setObjectName("cBox_Port")
        self.Layout_controls.addWidget(self.cBox_Port, 0, 1, 1, 1)
        self.cBox_Source = QtWidgets.QComboBox(self.centralwidget)
        self.cBox_Source.setObjectName("cBox_Source")
        self.Layout_controls.addWidget(self.cBox_Source, 0, 0, 1, 1)

        self.pButton_Start = QtWidgets.QPushButton(self.centralwidget)
        self.pButton_Start.setMinimumSize(QtCore.QSize(0, 0))
        self.pButton_Start.setObjectName("pButton_Start")
        self.Layout_controls.addWidget(self.pButton_Start, 0, 3, 1, 1)


        self.pButton_Params = QtWidgets.QPushButton(self.centralwidget)
        self.pButton_Params.setMinimumSize(QtCore.QSize(0, 0))
        self.pButton_Params.setObjectName("pButton_Params")
        self.Layout_controls.addWidget(self.pButton_Params, 0, 4, 1, 1)
        self.pButton_Params.setText("Open Param File")

        self.pButton_Update = QtWidgets.QPushButton(self.centralwidget)
        self.pButton_Update.setMinimumSize(QtCore.QSize(0, 0))
        self.pButton_Update.setObjectName("pButton_Update")
        self.Layout_controls.addWidget(self.pButton_Update, 0, 5, 1, 1)
        self.pButton_Update.setText("Update")

        self.paramAddr = QtWidgets.QLineEdit(self.centralwidget)
        self.paramAddr.setObjectName("paramAddr")
        self.paramAddr.setPlaceholderText("Address in Hex")
        self.Layout_controls.addWidget(self.paramAddr, 4, 1, 1, 1)

        self.paramData = QtWidgets.QLineEdit(self.centralwidget)
        self.paramData.setObjectName("paramData")
        self.paramData.setPlaceholderText("Data in int")
        self.Layout_controls.addWidget(self.paramData, 4, 2, 1, 1)       

        self.paramSet = QtWidgets.QPushButton(self.centralwidget)
        self.paramSet.setObjectName("paramSet")
        self.Layout_controls.addWidget(self.paramSet, 4, 3, 1, 1)

        self.paramGet = QtWidgets.QPushButton(self.centralwidget)
        self.paramGet.setObjectName("paramGet")
        self.Layout_controls.addWidget(self.paramGet, 4, 4, 1, 1)

        self.paramGetData = QtWidgets.QLineEdit(self.centralwidget)
        self.paramGetData.setObjectName("paramGetData")
        self.Layout_controls.addWidget(self.paramGetData, 4, 5, 1, 1)

        self.paramAddrTxt = QtWidgets.QLabel("PARAM")
        self.paramAddrTxt.setObjectName("paramAddrTxt")
        self.Layout_controls.addWidget(self.paramAddrTxt, 4, 0, 1, 1)

        self.VarAddr = QtWidgets.QLineEdit(self.centralwidget)
        self.VarAddr.setObjectName("VarAddr")
        self.VarAddr.setPlaceholderText("Address in Hex")
        self.Layout_controls.addWidget(self.VarAddr, 5, 1, 1, 1)

        self.VarData = QtWidgets.QLineEdit(self.centralwidget)
        self.VarData.setObjectName("VarData")
        self.VarData.setPlaceholderText("Data in int")
        self.Layout_controls.addWidget(self.VarData, 5, 2, 1, 1)        

        self.VarSet = QtWidgets.QPushButton(self.centralwidget)
        self.VarSet.setObjectName("VarSet")
        self.Layout_controls.addWidget(self.VarSet, 5, 3, 1, 1)

        self.VarGet = QtWidgets.QPushButton(self.centralwidget)
        self.VarGet.setObjectName("VarGet")
        self.Layout_controls.addWidget(self.VarGet, 5, 4, 1, 1)

        self.VarGetData = QtWidgets.QLineEdit(self.centralwidget)
        self.VarGetData.setObjectName("VarGetData")
        self.Layout_controls.addWidget(self.VarGetData, 5, 5, 1, 1)

        self.VarAddrTxt = QtWidgets.QLabel("VAR")
        self.VarAddrTxt.setObjectName("VarAddrTxt")
        self.Layout_controls.addWidget(self.VarAddrTxt, 5, 0, 1, 1)


        self.StrmAddr1 = QtWidgets.QLineEdit(self.centralwidget)
        self.StrmAddr1.setObjectName("StrmAddr1")
        self.StrmAddr1.setPlaceholderText("Stream 1 Addr")
        self.Layout_controls.addWidget(self.StrmAddr1, 6, 0, 1, 1)

        self.StrmAddr2 = QtWidgets.QLineEdit(self.centralwidget)
        self.StrmAddr2.setObjectName("StrmAddr2")
        self.StrmAddr2.setPlaceholderText("Stream 2 Addr")
        self.Layout_controls.addWidget(self.StrmAddr2, 6, 1, 1, 1)

        self.StrmAddr3 = QtWidgets.QLineEdit(self.centralwidget)
        self.StrmAddr3.setObjectName("StrmAddr3")
        self.StrmAddr3.setPlaceholderText("Stream 3 Addr")
        self.Layout_controls.addWidget(self.StrmAddr3, 6, 2, 1, 1)        

        self.StrmAddr4 = QtWidgets.QLineEdit(self.centralwidget)
        self.StrmAddr4.setObjectName("StrmAddr4")
        self.StrmAddr4.setPlaceholderText("Stream 4 Addr")
        self.Layout_controls.addWidget(self.StrmAddr4, 6, 3, 1, 1)

        self.StrmAddr5 = QtWidgets.QLineEdit(self.centralwidget)
        self.StrmAddr5.setObjectName("StrmAddr5")
        self.StrmAddr5.setPlaceholderText("Stream 5 Addr")
        self.Layout_controls.addWidget(self.StrmAddr5, 6, 4, 1, 1)

        self.StreamTime= QtWidgets.QLineEdit(self.centralwidget)
        self.StreamTime.setObjectName("SetupStream")
        self.StreamTime.setPlaceholderText("Stream Time Interval")
        self.Layout_controls.addWidget(self.StreamTime, 6, 5, 1, 1)

        self.SetupStream = QtWidgets.QPushButton(self.centralwidget)
        self.SetupStream.setObjectName("SetupStream")
        self.Layout_controls.addWidget(self.SetupStream, 6, 6, 1, 1)

        # self.AddrTxt = QtWidgets.QLabel("   ADDRESS")
        # self.AddrTxt.setObjectName("AddrTxt")
        # self.Layout_controls.addWidget(self.AddrTxt, 3, 1, 1, 1)

        # self.DataTxt = QtWidgets.QLabel("   DATA")
        # self.DataTxt.setObjectName("DataTxt")
        # self.Layout_controls.addWidget(self.DataTxt, 3, 2, 1, 1)

        self.sBox_Samples = QtWidgets.QSpinBox(self.centralwidget)
        self.sBox_Samples.setMinimum(1)
        self.sBox_Samples.setMaximum(100000)
        self.sBox_Samples.setProperty("value", 500)
        self.sBox_Samples.setObjectName("sBox_Samples")
        self.Layout_controls.addWidget(self.sBox_Samples, 0, 2, 1, 1)
        self.chBox_export = QtWidgets.QCheckBox(self.centralwidget)
        self.chBox_export.setEnabled(True)
        self.chBox_export.setObjectName("chBox_export")
        self.Layout_controls.addWidget(self.chBox_export, 1, 2, 1, 1)
        self.gridLayout.addLayout(self.Layout_controls, 7, 0, 1, 2)
        self.Layout_graphs = QtWidgets.QGridLayout()
        self.Layout_graphs.setObjectName("Layout_graphs")
        self.plt = GraphicsLayoutWidget(self.centralwidget)
        self.plt.setAutoFillBackground(False)
        self.plt.setStyleSheet("border: 0px;")
        self.plt.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.plt.setFrameShadow(QtWidgets.QFrame.Plain)
        self.plt.setLineWidth(0)
        self.plt.setObjectName("plt")
        self.Layout_graphs.addWidget(self.plt, 0, 0, 1, 1)
        self.gridLayout.addLayout(self.Layout_graphs, 2, 1, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "CharaUI"))
        self.pButton_Stop.setText(_translate("MainWindow", "Stop"))
        self.pButton_Start.setText(_translate("MainWindow", "Start"))

        self.paramSet.setText(_translate("MainWindow", "Param Set"))
        self.paramGet.setText(_translate("MainWindow", "Param Get"))
        self.VarSet.setText(_translate("MainWindow", "Var Set"))
        self.VarGet.setText(_translate("MainWindow", "Var Get"))

        self.SetupStream.setText(_translate("MainWindow", "Update Stream"))

        self.sBox_Samples.setSuffix(_translate("MainWindow", " samples"))

        self.chBox_export.setText(_translate("MainWindow", "Export to CSV"))

from pyqtgraph import GraphicsLayoutWidget
