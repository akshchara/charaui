import multiprocessing
from time import sleep

from charaUI.core.constants import Constants
from charaUI.common.logger import Logger as Log
from charaUI.common.target_logger import Target_logger as Target_log
from array import array
from queue import Queue
from charaUI.processors.Serial import SerialProcess


TAG = "Parser"

header_received = 0
length_received = 0
length = 0
data_pkt = array('i')


seq_num = 0

class ParserProcess(multiprocessing.Process):
    """
    Process to parse incoming data, parse it, and then distribute it to graph and storage.
    """
    def __init__(self, plotData_queue, ParamDataQueue, varDataQueue, store_reference=None,
                 split=Constants.csv_delimiter,
                 consumer_timeout=Constants.parser_timeout_ms):
        """

        :param data_queue: Reference to Queue where processed data will be put.
        :type plotData_queue: multiprocessing Queue.
        :param store_reference: Reference to CSVProcess instance, if needed.
        :type store_reference: CSVProcess (multiprocessing.Process)
        :param split: Delimiter in incoming data.
        :type split: str.
        :param consumer_timeout: Time to wait after emptying the internal buffer before next parsing.
        :type consumer_timeout: float.
        """
        multiprocessing.Process.__init__(self)
        self._exit = multiprocessing.Event()
        self._in_queue = multiprocessing.Queue()
        # Initializing a queue
        self._rxData_queue = Queue(maxsize = 300)
        self._out_queue = plotData_queue
        self.ParamDataQueue = ParamDataQueue
        self.varDataQueue = varDataQueue
        self._consumer_timeout = consumer_timeout
        self._split = split
        self._store_reference = store_reference
        header_received = 0
        Log.d(TAG, "Process ready")

    def add(self, txt):
        """
        Adds new raw data to internal buffer.
        :param txt: Raw data comming from acquisition process.
        :type txt: basestring.
        :return:
        """
        self._in_queue.put(txt)

    def run(self):
        """
        Process will monitor the internal buffer to parse raw data and distribute to graph and storage, if needed.
        The process will loop again after timeout if more data is available.
        :return:
        """
        Log.d(TAG, "Process starting...")
        self.pktCounter = 0
        while not self._exit.is_set():
            self._consume_queue()
            sleep(0.001)
        # last check on the queue to completely remove data.
        self._consume_queue()
        Log.d(TAG, "Process finished")

    def stop(self):
        """
        Signals the process to stop parsing data.
        :return:
        """
        Log.d(TAG, "Process finishing...")
        self._exit.set()

    def _consume_queue(self):
        """
        Consumer method for the queues/process.
        Used in run method to recall after a stop is requested, to ensure queue is emptied.
        :return:
        """
        
        while not self._in_queue.empty():
            queue = self._in_queue.get(timeout=self._consumer_timeout)
            self._parse_csv(queue[0], queue[1])

    def _parse_csv(self, time, line):
        """
        Parses incoming data and distributes to external processes.
        :param time: Timestamp.
        :type time: float.
        :param line: Raw data coming from acquisition process.
        :type line: basestring.
        :return:
        """
        global header_received, length_received, length, data_pkt
        tlogstr = ""
        if len(line) > 0:
            split_int = [line[i] for i in range (0, len(line))]
            for i in range (0, len(line)):
                self._rxData_queue.put(split_int[i])

            while not self._rxData_queue.empty():

                if (header_received == 0):
                    rx_byte = self._rxData_queue.get(timeout=self._consumer_timeout)
                    if (rx_byte != 0xAB):
                        header_received = 0
                        #print ("HEADER MISMATCH | PURGING", rx_byte)
                    else:
                        header_received = 1
                elif (length_received == 0):
                    if (self._rxData_queue.empty() != True):
                        length = self._rxData_queue.get(timeout=self._consumer_timeout)
                        length_received = 1;
                        data_pkt.append(0xAB)
                        data_pkt.append(length)
                else:
                    data_pkt.append(self._rxData_queue.get(timeout=self._consumer_timeout))

                    if (length <= len(data_pkt)-2):
                        header_received = 0  
                        length_received = 0

                        seq_num = data_pkt[2]
                        pkt_type = data_pkt[3]

                        if (pkt_type == 0x30):
                            d1 = int((data_pkt[4]<<24 | data_pkt[5]<<16 | data_pkt[6]<<8 | data_pkt[7]))
                            d2 = int((data_pkt[8]<<24 | data_pkt[9]<<16 | data_pkt[10]<<8 | data_pkt[11]))
                            d3 = int((data_pkt[12]<<24 | data_pkt[13]<<16 | data_pkt[14]<<8 | data_pkt[15]))
                            d4 = int((data_pkt[16]<<24 | data_pkt[17]<<16 | data_pkt[18]<<8 | data_pkt[19]))
                            d5 = int((data_pkt[20]<<24 | data_pkt[21]<<16 | data_pkt[22]<<8 | data_pkt[23]))

                            if (d1 > 2**31):
                                d1 = d1 - 2**32
                            if (d2 > 2**31):
                                d2 = d2 - 2**32
                            if (d3 > 2**31):
                                d3 = d3 - 2**32
                            if (d4 > 2**31):
                                d4 = d4 - 2**32
                            if (d5 > 2**31):
                                d5 = d5 - 2**32
                                
                            self.pktCounter = self.pktCounter+1;


                            values = [d1,d2,d3,d4,d5]
                            self._out_queue.put((self.pktCounter, values))
                            if self._store_reference is not None:
                               self._store_reference.add(time, values)

                        elif (pkt_type == 0x10):
                            d1 = (data_pkt[6]<<24 | data_pkt[7]<<16 | data_pkt[8]<<8 | data_pkt[9])
                            if (d1 > 2**31):
                                d1 = d1 - 2**32
                            self.ParamDataQueue.put(d1)


                        elif (pkt_type == 0x20):
                            d1 = (data_pkt[6]<<24 | data_pkt[7]<<16 | data_pkt[8]<<8 | data_pkt[9])
                            if (d1 > 2**31):
                                d1 = d1 - 2**32
                            self.varDataQueue.put(d1)

                        elif (pkt_type == 0x40):
                            for i in range (4, len(data_pkt)-1):
                                tlogstr += chr(data_pkt[i])
                            Target_log.tlog(TAG, tlogstr)
                        else:
                            Log.d(TAG, "ERROR | PACKET UNIDENTIFIED")
                            data_pkt = array('i')    

                        data_pkt = array('i')     

    def send_get_variable_pkt(self, addr):
        global seq_num
        datapkt = array("B") 
        datapkt.append(0xAB)
        datapkt.append(0x05)
        datapkt.append(seq_num)
        datapkt.append(0x20)
        datapkt.append(addr>>8)
        datapkt.append(addr & 0xFF)
        datapkt.append(0xFF)
        if (seq_num == 254):
            seq_num = 0
        seq_num += 1
        # Log.d(TAG, "SEND GET VAR PKT {}".format(datapkt))
        SerialProcess.serialSendPkt(datapkt)

    def send_get_param_pkt(self, addr):
        global seq_num
        datapkt = array("B")     
        datapkt.append(0xAB)
        datapkt.append(0x05)
        datapkt.append(seq_num)
        datapkt.append(0x10)
        datapkt.append(addr>>8)
        datapkt.append(addr & 0xFF)
        datapkt.append(0xFF)
        if (seq_num == 254):
            seq_num = 0
        seq_num += 1
        # Log.d(TAG, "SEND GET PARAM PKT {}".format(datapkt))
        SerialProcess.serialSendPkt(datapkt)

    def send_set_variable_pkt(self, addr, data):
        global seq_num
        datapkt = array("B") 
        datapkt.append(0xAB)
        datapkt.append(0x09)
        datapkt.append(seq_num)
        datapkt.append(0x21)
        datapkt.append(addr>>8)
        datapkt.append(addr & 0xFF)
        datapkt.append((data>>24) & 0xFF);
        datapkt.append((data>>16) & 0xFF);
        datapkt.append((data>>8) & 0xFF);
        datapkt.append(data & 0xFF)
        datapkt.append(0xFF)
        if (seq_num == 254):
            seq_num = 0
        seq_num += 1
        # Log.d(TAG, "SEND SET VAR PKT {}".format(datapkt))
        SerialProcess.serialSendPkt(datapkt)

    def send_set_param_pkt(self, addr, data):
        
        global seq_num
        datapkt = array("B")     
        datapkt.append(0xAB)
        datapkt.append(0x09)
        datapkt.append(seq_num)
        datapkt.append(0x11)
        datapkt.append(addr>>8)
        datapkt.append(addr & 0xFF)
        datapkt.append((data>>24) & 0xFF )
        datapkt.append((data>>16) & 0xFF)
        datapkt.append((data>>8) & 0xFF)
        datapkt.append(data & 0xFF)
        datapkt.append(0xFF)
        if (seq_num == 254):
            seq_num = 0
        seq_num += 1
        Log.i(TAG, "SEND SET PARAM PKT {}".format(datapkt))
        SerialProcess.serialSendPkt(datapkt) 

    def send_setup_stream_pkt(self, addr1, addr2, addr3, addr4, addr5, timeperiod):
        global seq_num
        datapkt = array("B")     
        datapkt.append(0xAB)
        datapkt.append(0x0F)
        datapkt.append(seq_num)
        datapkt.append(0x31)
        datapkt.append(addr1>>8)
        datapkt.append(addr1 & 0xFF)
        datapkt.append(addr2>>8)
        datapkt.append(addr2 & 0xFF)
        datapkt.append(addr3>>8)
        datapkt.append(addr3 & 0xFF)
        datapkt.append(addr4>>8)
        datapkt.append(addr4 & 0xFF)
        datapkt.append(addr5>>8)
        datapkt.append(addr5 & 0xFF)
        datapkt.append(timeperiod>>8)
        datapkt.append(timeperiod & 0xFF)
        datapkt.append(0xFF)
        if (seq_num == 254):
            seq_num = 0
        seq_num += 1
        #Log.d(TAG, "SEND SETUP STREAM PKT {}".format(datapkt))  
        SerialProcess.serialSendPkt(datapkt)            
