import logging
import logging.handlers
import sys
from enum import Enum
import time


from charaUI.common.architecture import Architecture
from charaUI.common.fileManager import FileManager
from charaUI.core.constants import Constants


class Target_logger:
    """
    Wrapper for logging package.
    """
    def __init__(self, level, enable_console=False):
        """
        Creates file logging (as csv) and to console, if requested.
        :param level: Level to show in log.
        :type level: int.
        :param enable_console: Enabled logging to console.
        :type enable_console: bool,
        """
        target_log_format_file = logging.Formatter('%(asctime)s,%(levelname)s,%(message)s')
        log_format_console = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        self.targetlogger = logging.getLogger()
        self.targetlogger.setLevel(level.value)

        FileManager.create_dir(Constants.app_export_path)
        str_time = time.strftime("%H:%M:%S")
        target_file_handler = logging.handlers.RotatingFileHandler("{}/{}"
                                                            .format(Constants.app_export_path, Constants.target_log_filename+str_time),
                                                            maxBytes=Constants.target_log_max_bytes,
                                                            backupCount=0)
        target_file_handler.setFormatter(target_log_format_file)
        self.targetlogger.addHandler(target_file_handler)

        if enable_console:
            console_handler = logging.StreamHandler(sys.stdout)
            console_handler.setFormatter(log_format_console)
            self.targetlogger.addHandler(console_handler)

        self._show_user_info()

    @staticmethod
    def close():
        """
        Closes the enabled loggers.
        :return:
        """
        logging.shutdown()

    @staticmethod
    def d(tag, msg):
        """
        Logs at debug level.
        :param tag: TAG to identify the log.
        :type tag: str.
        :param msg: Message to log.
        :type msg: str.
        :return:
        """
        logging.debug("[{}] {}".format(str(tag), str(msg)))

    @staticmethod
    def i(tag, msg):
        """
        Logs at info level.
        :param tag: TAG to identify the log.
        :type tag: str.
        :param msg: Message to log.
        :type msg: str.
        :return:
        """
        logging.info("[{}] {}".format(str(tag), str(msg)))

    @staticmethod
    def w(tag, msg):
        """
        Logs at warning level.
        :param tag: TAG to identify the log.
        :type tag: str.
        :param msg: Message to log.
        :type msg: str.
        :return:
        """
        logging.warning("[{}] {}".format(str(tag), str(msg)))

    @staticmethod
    def e(tag, msg):
        """
        Logs at error level.
        :param tag: TAG to identify the log.
        :type tag: str.
        :param msg: Message to log.
        :type msg: str.
        :return:
        """
        logging.error("[{}] {}".format(str(tag), str(msg)))

    def tlog(tag, msg):
        logging.info("[{}] {}".format(str(tag), str(msg)))          

    @staticmethod
    def _show_user_info():
        """
        Logs at info level architecture related information.
        :return:
        """
        tag = "User"
        Target_logger.i(tag, "Platform: {}".format(Architecture.get_os_name()))
        Target_logger.i(tag, "Path: {}".format(Architecture.get_path()))
        Target_logger.i(tag, "Python: {}".format(Architecture.get_python_version()))


class LoggerLevel(Enum):
    """
    Enum for the Logger levels (wrappers for logging package levels).
    """
    CRITICAL = logging.CRITICAL
    ERROR = logging.ERROR
    WARNING = logging.WARNING
    INFO = logging.INFO
    DEBUG = logging.DEBUG
