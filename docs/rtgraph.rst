charaUI package
===============

Subpackages
-----------

.. toctree::

    charaUI.common
    charaUI.core
    charaUI.processors
    charaUI.ui

Submodules
----------

charaUI.CharaUI module
----------------------

.. automodule:: charaUI.CharaUI
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: charaUI
    :members:
    :undoc-members:
    :show-inheritance:
